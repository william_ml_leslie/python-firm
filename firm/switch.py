#   python-firm:  CFFI wrapper for the libFirm compiler backend.
#   Copyright (C) 2017  William ML Leslie
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301 USA
#
from firm.base import libfirm, BaseSequence
from firm.tarval import Tarval
from firm.types import MODE_BU


class _ProxySwitchEntry(object):
    def __init__(self, val, offset):
        self._ir_val = val
        self.offset = offset

    @property
    def min(self):
        return libfirm.ir_switch_table_get_min(self._ir_val, self.offset)

    @property
    def max(self):
        return libfirm.ir_switch_table_get_max(self._ir_val, self.offset)

    @property
    def projection(self):
        return libfirm.ir_switch_table_get_pn(self._ir_val, self.offset)

    def __iter__(self):
        return iter([self.min, self.max, self.projection])

    def __getitem__(self, index):
        return [self.min, self.max, self.projection][index]


class SwitchTable(BaseSequence):
    def __init__(self, val):
        self._ir_val = val

    @classmethod
    def new(cls, graph, num_entries):
        table = libfirm.ir_new_switch_table(graph._ir_graph, num_entries)
        return SwitchTable(table)

    def __len__(self):
        return libfirm.ir_switch_table_get_n_entries(self._ir_val)

    def getitem(self, index):
        return _ProxySwitchEntry(self._ir_val, index)

    def setitem(self, index, entry):
        vmin, vmax, pn = entry
        if not isinstance(vmin, Tarval):
            vmin = Tarval.from_int(vmin, MODE_BU)
        if not isinstance(vmax, Tarval):
            vmax = Tarval.from_int(vmax, MODE_BU)
        libfirm.ir_switch_table_set(self._ir_val, vmin, vmax, pn)


def switch_table(graph, entries, mode=None):
    table = SwitchTable.new(graph, len(entries))
    if mode is not None:
        for idx, (mn, mx, pn) in enumerate(entries):
            table[idx] = (Tarval.from_int(mn, mode),
                          Tarval.from_int(mx, mode), pn)
    else:
        for idx, entry in enumerate(entries):
            table[idx] = entry
    return table
        
