#   python-firm:  CFFI wrapper for the libFirm compiler backend.
#   Copyright (C) 2016-2020  William ML Leslie
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301 USA
#
"""Supported behaviours of the FIRM-provided types.

These are intentionally fairly C-like, including the restriction that
types cannot depend on themselves.

"""

import pytest
from firm.types import (Array, Method, Pointer, Struct, Union, Class,
                        INT_8, INT_32, UINT_32, FLOAT_64)
from firm import bitfields


def test_fixed_array():
    xs = Array.new(INT_32, 7)
    assert xs.element_type == INT_32
    assert xs.size == 7
    assert repr(xs) == 'Array(Primitive(Is), 7)'


def test_varying_array():
    xs = Array.new(INT_32)
    assert xs.element_type == INT_32
    assert xs.size == 0
    assert repr(xs) == 'Array(Primitive(Is), 0)'


def test_byte_array():
    xs = Array.new(INT_8, 7)
    assert xs.element_type == INT_8
    assert xs.size == 7
    assert repr(xs) == 'Array(Primitive(Bs), 7)'


def test_method():
    method = Method.new([INT_32, INT_8], [UINT_32, FLOAT_64])
    assert list(method.params) == [INT_32, INT_8]
    assert list(method.results) == [UINT_32, FLOAT_64]
    method.regparams = 1
    assert method.regparams == 1
    assert not method.is_variadic
    #assert method.calling_convention
    assert repr(method) == ('Method([Primitive(Is), Primitive(Bs)] -> '
                            '[Primitive(Iu), Primitive(D)])')


def test_variadic_method():
    method = Method.new([INT_32], [INT_32])
    method.variadic = 1
    assert method.variadic
    #assert method.calling_convention


def test_pointer():
    pointer = Pointer.new(INT_32)
    assert pointer.target == INT_32
    assert repr(pointer) == 'Pointer(Primitive(Is))'


def test_cyclic_pointer():
    """Recursive types aren't supported in C, so neither are they in firm.

    This is a shame to be honest - pointers make a great forward
    definition.

    """
    with pytest.raises(AssertionError):
        pointer = Pointer.new(None)


def test_struct_filled_out_after_pointer_taken():
    struct = Struct.new(b"wat")
    struct.size = 16
    struct.alignment = 8
    struct.state = bitfields.TypeState.FIXED
    pstruct = Pointer.new(struct)
    struct.add_field(b"data", INT_32, 0, 8)
    struct.add_field(b"next", pstruct, 8, 8)
    assert repr(pstruct) == "Pointer(Struct('wat'))"


def test_struct():
    struct = Struct.new(b"hello")
    struct.add_field(b"world", INT_32, 5, 8)
    # todo: add methods for listing and getting fields.
    assert struct[0].type == INT_32 # is this actually an entity?
    assert repr(struct) == "Struct('hello')"


def test_union():
    cls = Struct.new(b"class_attr")
    cls.add_field(b"n_methods", INT_32)
    meth = Struct.new(b"method_attr")
    meth.add_field(b"n_params", UINT_32)

    union = Union.new(b"attr")
    union.add_field(b"cls", cls, 0)
    union.add_field(b"meth", meth, 0)

    assert union[0].type == cls
    assert union[1].type == meth
    assert repr(union) == "Union('attr')"


def test_types_hashable():
    """Pointer types hash by target type.
    """
    p = Pointer.new(INT_32)
    px = Pointer.new(INT_32)
    pu = Pointer.new(UINT_32)
    assert hash(p) == hash(px)
    assert hash(p) != hash(pu)
