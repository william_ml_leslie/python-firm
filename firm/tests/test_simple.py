#   python-firm:  CFFI wrapper for the libFirm compiler backend.
#   Copyright (C) 2016-2020  William ML Leslie
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301 USA
#
"""Produce and verify some simple SSA graphs.
"""

import sys
from firm import types, function
from firm.base import libfirm, ffi
from firm.types import Method, INT_32, MODE_IS
from firm.function import Function, IRGraph
from firm.bitfields import Relation
from firm.tarval import Tarval


def test_simple():
    sig = Method.new([INT_32] * 3, [INT_32])
    func = Function.new(sig, b"simple")
    graph = IRGraph.new(func, 3)
    block = graph.current_block
    tarval_7 = Tarval.from_int(7, MODE_IS)
    v0 = block.op_Add(graph.arg(0, MODE_IS), graph.arg(1, MODE_IS))
    v1 = block.op_Add(graph.arg(1, MODE_IS), graph.arg(2, MODE_IS))
    v3 = block.op_Mul(v0, v1)
    v4 = block.op_Sub(v3, block.op_Const(tarval_7))
    block.op_return(v4)
    block.mature()
    graph.finalise()
    graph.verify()


def test_diamond_control():
    sig = Method.new([INT_32] * 3, [INT_32])
    func = Function.new(sig, b"diamond")
    graph = IRGraph.new(func, 3)

    block = graph.current_block
    tarval_7 = Tarval.from_int(7, MODE_IS)
    v0 = block.op_Add(graph.arg(0, MODE_IS), graph.arg(1, MODE_IS))
    v1 = block.op_Cmp(v0, graph.arg(2, MODE_IS), Relation.LESS)
    v2 = block.op_Cond(v1)

    b_true = graph.op_Block([v2.pn_true])
    b_false = graph.op_Block([v2.pn_false])

    v3 = b_true.op_Mul(v0, graph.arg(2, MODE_IS))
    tj = b_true.op_Jmp()

    v4 = b_false.op_Sub(v0, block.op_Const(tarval_7))
    fj = b_false.op_Jmp()

    ret_block = graph.op_Block([tj, fj])
    v5 = ret_block.op_Phi([v3, v4], MODE_IS)
    ret_block.op_return(v5)

    block.mature()
    b_true.mature()
    b_false.mature()
    ret_block.mature()

    graph.finalise()
    graph.verify()


def test_asm():
    sig = Method.new([INT_32] * 3, [INT_32])
    func = Function.new(sig, b"asm")
    graph = IRGraph.new(func, 3)

    block = graph.current_block
    register = libfirm.new_id_from_str(b'r')
    in_constraint = [0, 0, register, MODE_IS]
    both_constraint = [1, -1, register, MODE_IS]
    asm = block.op_ASM(
        [graph.arg(0, MODE_IS), graph.arg(2, MODE_IS)],
        b"firm %0, %1 /* do a thing */",
        [in_constraint, both_constraint],
        [libfirm.new_id_from_str(b'memory')],
        0
    )

    block.op_return(block.op_Proj(asm, MODE_IS, 0))
    block.mature()

    graph.finalise()
    graph.verify() # TODO:  check the comment got inserted into the .S
