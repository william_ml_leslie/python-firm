Python-Firm:  CFFI wrapper around the libFirm compiler backend
==============================================================

:Author: William ML Leslie
:Licence: GNU Lesser General Public Licence v2.1 or later

Firm_ is a library that provides a graph-based intermediate
representation, optimizations, and assembly code generation suitable
for use in both AOT and JIT compilers.

.. _Firm: http://pp.ipd.kit.edu/firm/

This is a wrapper for libfirm.  You can initialise and access the base
library like so::

  from firm.base import libfirm

In addition, most of the modules that libfirm provides have nice
pythonic wrappers.  You can get started by reviewing the tests in
firm.tests, test_simple for example considers the API of using the
basic wrapper objects directly.

There is a package containing a Tutorial_ language, showing how to
compile most C-like constructs, though it is very much a work in
progress.

.. _Tutorial: https://bitbucket.org/william_ml_leslie/firm-tutorial/src/master/

Our process has been to get enough of the library working to test the
next step, so there is definitely some functionality from libfirm that
we are missing.  For example, there is currently no access to gcc
builtins.  Nevertheless, the functionality provided should be enough
to compile your SSA graphs to any supported platform.

TODO
----

* Make hooking in assemblers and linkers easier

* Wrap Builtins

* Make it possible to write backends in python

* Build wheels

Building / Installing
---------------------

If you have the libfirm package and its headers installed, you can
simply use the source pip package::

  pip install python-firm

Don't forget to add ``--user`` if you are not using a virtualenv.

If you want to build libfirm and python-firm yourself from scratch,
clone this repository with the ``--recursive`` git option to obtain
the libfirm source.  In addition to python, you will need Perl, make,
and an ANSI C99 compiler to build libfirm.  Then::

  $ cd libfirm/
  $ make
  $ cd ..

You can build python-firm for a specific libfirm with

::

  python -m gen.compile

Or if you want to build and install,

::

  python -m pip install .

If you want to build the module for a libfirm you've built locally,
set the environment variable FIRM_HOME.  It will try to statically
link libfirm if possible; if you want to use a dynamically linked
libfirm you will need to arrange for it to be on the linker's library
path.

The gen module also contains templates for use when the ir_spec
changes::

  python $FIRM_HOME/scripts/gen_ir.py gen/pythonbits.py gen/operation_template.py > firm/operations.py
  python $FIRM_HOME/scripts/gen_ir.py gen/pythonbits.py gen/defs_template.h > firm/node_defs.h

Tests
-----

You will need pytest to run the tests.  Use the tests to get a basic
understanding of how to interact with the library.  Notably,
test_simple should give you a basic understanding of how to build
functions in Firm.

For more advanced usage, see the `firm-tutorial`_ repository.

.. _firm-tutorial: https://bitbucket.org/william_ml_leslie/firm-tutorial

The tests can be run with::

  pytest firm/tests/
