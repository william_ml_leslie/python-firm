{{warning}}

typedef enum {{spec.name}}_opcode {
  {%- for node in nodes %}
        {{spec.name}}o_{{node.name}},
  {%- endfor %}
} {{spec.name}}_opcode;

{%- for node in nodes %}
{%- if node.constructor %}

ir_node *new_rd_{{node.name}}(
        {%- filter parameters %}
                dbg_info *dbgi
                {{node|blockparameter}}
                {{node|nodeparameters}}
        {% endfilter %});
{%- endif %}

{% for input in node.ins -%}
ir_node *get_{{node.name}}_{{input.name}}(const ir_node *node);
void set_{{node.name}}_{{input.name}}(ir_node *node, ir_node *{{input.name|escape_keywords}});
{% endfor -%}
{%- if node.input_name -%}
int get_{{node.name}}_n_{{node.input_name}}s(ir_node const *node);
ir_node *get_{{node.name}}_{{node.input_name}}(ir_node const *node, int pos);
void set_{{node.name}}_{{node.input_name}}(ir_node *node, int pos, ir_node *{{node.input_name}});
ir_node **get_{{node.name}}_{{node.input_name}}_arr(ir_node *node);
{%- endif %}
{%- for attr in node.attrs|hasnot("noprop") %}
{{attr.type}} get_{{node.name}}_{{attr.name}}(const ir_node *node);
{% endfor -%}
ir_op *get_op_{{node.name}}(void);
{%- endfor %}
