#   python-firm:  CFFI wrapper for the libFirm compiler backend.
#   Copyright (C) 2016-2020  William ML Leslie
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301 USA
#
import os
import glob
from cffi import FFI
from os.path import join as path_join

def here(*name):
    return path_join(os.path.dirname(__file__), *name)

library_dirs = []
include_dirs = []

FIRM_PROVIDED = os.path.exists(here('../libfirm/Makefile'))
FIRM_HOME = os.environ.get('FIRM_HOME', False)
obj_path = None

if FIRM_HOME:
    for variant in 'debug profile coverage optimise'.split():
        library_dirs.append(path_join(FIRM_HOME, 'build', variant))
        libfirm_a = path_join(FIRM_HOME, 'build', variant, 'libfirm.a')
        if os.path.exists(libfirm_a) and obj_path is None:
            obj_path = libfirm_a
    for loc in 'include build/gen/include build/gen/include/libfirm'.split():
        include_dirs.append(path_join(FIRM_HOME, loc))
elif FIRM_PROVIDED:
    obj_path = '%(build_dir)s/%(variant)s/libfirm.a'
    include_dirs = ['%(build_dir)s/gen/include/',
                    '%(build_dir)s/gen/include/libfirm/',
                    here('../libfirm/include/')]

includes = '''
#include "libfirm/firm.h"
#include "libfirm/jit.h"
'''

ffi = FFI()
if obj_path:
    ffi.set_source("_python_firm", includes,
                   extra_objects = [obj_path],
                   include_dirs = include_dirs)
else:
    ffi.set_source("_python_firm", includes,
                   libraries = ['firm'],
                   library_dirs = library_dirs,
                   include_dirs = include_dirs)


#with open(here('fn_defs.h')) as f:
#    defs = list(f)
with open(here('firmtypes.h')) as f:
    defs = list(f)
with open(here('node_defs.h')) as f:
    defs.extend(f)

for filename in sorted(glob.glob(here('symbols/*.h'))):
    with open(filename) as f:
        defs.extend(f)

ffi.cdef('\n'.join(defs))

if __name__ == '__main__':
    ffi.compile()
