ir_graph* get_const_code_irg(void);
void lower_highlevel(void);
void lower_highlevel_graph(ir_graph *graph);
void be_lower_for_target(void);
void be_main(FILE *output, const char *compilation_unit_name);
void dump_graph_as_text(FILE *out, const ir_graph *graph);
void dump_ir_graph_file(FILE *out, ir_graph *graph);
ir_entity* ir_get_global(ident *name);
