ir_jit_segment_t *be_new_jit_segment(void);
void be_destroy_jit_segment(ir_jit_segment_t *segment);
void be_jit_set_entity_addr(ir_entity *entity, void const *address);
void const *be_jit_get_entity_addr(ir_entity const *entity);
ir_jit_function_t *be_jit_compile(ir_jit_segment_t *segment, ir_graph *irg);
unsigned be_get_function_size(ir_jit_function_t const *function);
void be_emit_function(char *buffer, ir_jit_function_t *function);

