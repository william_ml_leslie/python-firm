#   python-firm:  CFFI wrapper for the libFirm compiler backend.
#   Copyright (C) 2016-2020  William ML Leslie
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301 USA
#
import irops
import ir_spec
import jinjautil
import keyword

representation = {
    'ir_tarval*' : ('wrap.tarval', '%s._ir_val'),
    'ir_entity*' : ('wrap.entity', '%s._ir_entity'),
    'ir_type*' : ('wrap.type', '%s._ir_type'),
    'ident*' : ('#', 'libfirm.new_id_from_str(%s)'),
    'ident**' : ('#', 'ffi.new("ident*[]", list(map(libfirm.new_id_from_str, %s)))'),
}

enum_kinds = [
    'ir_builtin_kind', 'ir_relation'
]

jinjautil.export(representation, 'representation')

@jinjautil.export_filter
def pyargs(node):
    return ', '.join(get_python_args(node))


@jinjautil.export_filter
def pyparams(node):
    return ', '.join(get_python_args(node, True))


@jinjautil.export_filter
def base(node):
    bases = [b.__name__ for b in node.mro() if irops.is_abstract(b)]
    if bases:
        return ', '.join(bases)
    return 'BaseNode'


jinjautil.export("# Warning: this module is generated code.", "pywarning")
    

def unkeyword(name):
    if keyword.iskeyword(name):
        return name + '_'
    return name


jinjautil.export_filter(unkeyword)


assertions = {
    "Member" : [("entity.owner.state == bitfields.TypeState.FIXED",
                 repr("Indexing into a non-fixed struct."))]
}

output_mode = {
    "M" : "MODE_M",
    "P_frame_base" : "MODE_P",
    "T_args" : "MODE_T",
    "T_result" : "MODE_T",
    "X_regular" : "MODE_X",
    "X_except" : "MODE_X",
    "false" : "MODE_X",
    "true" : "MODE_X",
    "default" : "MODE_X",
    "X" : "MODE_X",
}

output_mode_opname = {
    ('Load', 'res') : 'mode',
    ('Div', 'res') : 'resmode',
    ('Mod', 'res') : 'resmode',
}

memory_policies = {
    'Alloc' : 'alignment',
    'ASM' : 'constraints, clobbers',
    'Builtin' : 'args',
    'Call' : 'irn_ptr, args',
    'CopyB' : 'irn_dst, irn_src, type, volatility',
    'Div' : 'irn_left, irn_right',
    'Free' : 'irn_ptr',
    'Load' : 'irn_ptr',
    'Mod' : 'irn_left, irn_right',
    'Raise' : 'irn_exo_ptr',
    'Return' : 'args',
    'Store' : 'irn_ptr'
}


jinjautil.export(assertions, 'assertions')
jinjautil.export(output_mode, 'output_mode')
jinjautil.export(output_mode_opname, 'output_mode_opname')
jinjautil.export(memory_policies, 'memory_policies')


def get_python_args(node, is_param=False):
    if is_param:
        # operations may be tied to a block or graph.
        if not node.block:
            yield 'self.block._ir_block'
        elif node.usesGraph:
            yield 'self.graph._ir_graph'

    optional_args = []
    for arg in node.ins:
        if is_param:
            yield 'irn_%s._ir_node' % (arg.name,)
        elif arg.name == 'mem':
            # memory doesn't need to be explicitly supplied - but if
            # possible, make it an optional argument.
            if node.arity not in ('variable', 'dynamic'):
                optional_args.append(('irn_mem', None))
        else:
            yield 'irn_' + arg.name

    if node.arity in ('variable', 'dynamic'):
        if is_param:
            yield 'len(args)'
            yield 'arg_array'
        else:
            yield 'args'
    if node.mode is None:
        yield 'mode'
    elif is_param:
        pass

    all_names = set(attr.name for attr in node.attrs)
    for attr in node.attrs:
        name = unkeyword(attr.name)
        if attr.init is not None:
            # these attributes are initialised within the C code.
            continue
        if name.startswith('n_') and name[2:] in all_names:
            # denotes array size of another param. infer & supply.
            if is_param:
                yield 'len(%s)' % (name[2:],)
        elif not is_param:
            # we'll need to get it from the caller.
            yield name
        elif attr.type in representation:
            # this has a custom representation.
            yield representation[attr.type][1] % name
        elif attr.type in enum_kinds:
            # it's an enum - get its value.
            yield '%s.value' % name
        else:
            yield name

    if node.pinned == 'exception' and node.pinned_init is None:
        if is_param:
            yield 'pinned'
        else:
            yield 'pinned=False'

    # additional arguments required by the node constructor.
    for arg in node.constructor_args:
        yield unkeyword(arg.name)

    # optional keyword arguments.
    for arg, default in optional_args:
        yield '%s=%r' % (arg, default)
