#   python-firm:  CFFI wrapper for the libFirm compiler backend.
#   Copyright (C) 2016-2020  William ML Leslie
#
#   This library is free software; you can redistribute it and/or
#   modify it under the terms of the GNU Lesser General Public
#   License as published by the Free Software Foundation; either
#   version 2.1 of the License, or (at your option) any later version.
#
#   This library is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
#   Lesser General Public License for more details.
#
#   You should have received a copy of the GNU Lesser General Public
#   License along with this library; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301 USA
#
{{pywarning}}

from firm.base import libfirm, ffi, BaseSequence
from firm.node import BaseNode, Binop, EntConst, TypeConst
from firm import types, wrap, bitfields


class NodeBuilder(object):
    {%- for node in nodes %}

    def op_{{node.name}}(self, {{node|pyargs}}):
        """{{node.__doc__}}
    {% for arg in node.arguments %}
        @param {{arg.name}} {{arg.comment}}
    {%- endfor %}
        """
        {%- for assertion, comment in assertions.get(node.name, ()) %}
        assert {{assertion}}, (
          {{comment}})
        {%- endfor %}
        {%- if node.name in memory_policies %}
        {%- if node.arity not in ('variable', 'dynamic') %}
        if irn_mem is None:
            irn_mem = self.memory_policy.{{node.name.lower() | unkeyword}}({{memory_policies[node.name]}})
        {%- else %}
        irn_mem = self.memory_policy.{{node.name.lower() | unkeyword}}({{memory_policies[node.name]}})
        {%- endif %}
        {%- endif %}
        {%- if node.arity in ('variable', 'dynamic') %}
        arg_array = ffi.new('ir_node *[]', [arg._ir_node for arg in args])
        {%- endif %}
        res = libfirm.new_rd_{{node.name}}(self._debug,
            {{node|pyparams}})
        {%- if 'uses_memory' in node.flags %}

        op = {{node.name}}(res)
        self.memory_policy.post_{{node.name.lower()}}(op)
        return op
        {%- else %}

        return {{node.name}}(res)
        {%- endif %}
    {%- endfor %}
{%- for node in nodes %}
{%- if node.input_name %}

class _{{node.name}}_{{node.input_name}}(BaseSequence):
    def __len__(self):
        return libfirm.get_{{node.name}}_n_{{node.input_name}}s(self._ir_val)

    def getitem(self, index):
        return libfirm.get_{{node.name}}_{{node.input_name}}(
            self._ir_val, index)

    def setitem(self, index, value):
        libfirm.set_{{node.name}}_{{node.input_name}}_(
            self._ir_val, index, value)
{%- endif %}

class {{node.name}}({{node|base}}):
    """{{node.__doc__}}
    """
    OPCODE = libfirm.get_op_code(libfirm.get_op_{{node.name}}())

    {%- for input in node.ins %}
    def getter(self):
        return wrap.node(libfirm.get_{{node.name}}_{{input.name}}(self._ir_node))

    def setter(self, value):
        libfirm.get_{{node.name}}_{{input.name}}(self._ir_node, value._ir_node)

    {{input.name}} = property(getter, setter)
    del getter, setter
    {%- endfor %}
    {%- if node.input_name %}
    @property
    def {{node.input_name}}(self):
        return _{{node.name}}_{{node.input_name}}(self._ir_node)
    {%- endif %}
    {%- for out in node.outs %}

    PN_{{out.name}} = {{loop.index0}}
    {%- if out.name in output_mode %}

    @property
    def pn_{{out.name}}(self):
        """{{out.comment}}
        """
        return self.block.op_Proj(self, types.{{output_mode[out.name]}}, {{loop.index0}})
    {%- elif (node.name, out.name) in output_mode_opname %}

    @property
    def pn_{{out.name}}(self):
        """{{out.comment}}
        """
        return self.block.op_Proj(self, self.{{output_mode_opname[node.name, out.name]}}, {{loop.index0}})
    {%- else %}

    def get_pn_{{out.name}}(self, mode):
        """{{out.comment}}
        """
        return self.block.op_Proj(self, mode, {{loop.index0}})
    {%- endif %}
    {%- endfor %}
    {%- for attr in node.attrs|hasnot("noprop") %}
    @property
    def {{attr.name}}(self):
        # of type: {{attr.type}}
        {%- if attr.type in representation %}
        return {{representation[attr.type][0]}}(libfirm.get_{{node.name}}_{{attr.name}}(self._ir_node))
        {%- else %}
        return libfirm.get_{{node.name}}_{{attr.name}}(self._ir_node)
        {%- endif %}
    {%- endfor %}
{%- endfor %}

by_opcode = {
{%- for node in nodes %}
    {{node.name}}.OPCODE : {{node.name}},
{%- endfor %}
}

import firm.extras # some custom field overrides
